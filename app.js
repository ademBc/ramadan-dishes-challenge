const express = require("express");
const { getCookTime, suggest } = require("./controllers/dishesController");
const app = express();
app.get("/cooktime", getCookTime);
app.get("/suggest", suggest);
module.exports = app;
