const { convertToMinute } = require("../helper/calcTime");

test("convert (hh:mm) format to minute", () => {
  expect(convertToMinute("15:15")).toBe(915);
});
test("convert (hh:mm) format to minute", () => {
  expect(convertToMinute("00:00")).toBe(0);
});
test("convert (hh:mm) format to minute", () => {
  expect(convertToMinute("19:59")).toBe(1199);
});
