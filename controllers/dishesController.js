const dishesData = require("../data/dishes");
const axios = require("axios").default;
const { calcTime } = require("../helper/calcTime");
const { calcScore } = require("../helper/calcDishesScore");
let data;
const getPrayerTimeData = async () => {
  // will be dynamic later
  const lat = 35.815806;
  const lan = 10.5971471;
  const res = await axios.get(
    `http://api.aladhan.com/v1/hijriCalendar?latitude=${lat}&longitude=${lan}&method=1&month=9&year=1443`
  );
  data = res.data.data;
};
getPrayerTimeData();
exports.getCookTime = (req, res, next) => {
  const { ingredient, day } = req.query;
  if (!ingredient || !day)
    return res.status(400).json({
      code: 400,
      status: "fail",
      message: "day and ingredient are required",
    });
  if (
    typeof ingredient !== "string" ||
    day.length > 2 ||
    isNaN(day) ||
    +day > 30 ||
    +day < 1
  )
    return res.status(400).json({
      code: 400,
      status: "fail",
      message: "invalid inputs",
    });
  const dishesWithIngredient = dishesData
    .filter((ele) => ele.ingredients.includes(ingredient))
    .map((ele) => {
      const cooktime = calcTime(ele.duration, day, data);
      return {
        name: ele.name,
        ingredients: ele.ingredients,
        cooktime,
      };
    });
  res.json(dishesWithIngredient);
};
exports.suggest = (req, res, next) => {
  const { day } = req.query;
  if (!day)
    return res.status(400).json({
      code: 400,
      status: "fail",
      message: "day is required",
    });
  if (day.length > 2 || isNaN(day) || +day > 30 || +day < 1)
    return res.status(400).json({
      code: 400,
      status: "fail",
      message: "invalid inputs",
    });
  let bestFood = dishesData[0];
  for (let i = 1; i < dishesData.length; i++) {
    if (calcScore(dishesData[i], day) > calcScore(bestFood, day)) {
      bestFood = dishesData[i];
    }
  }
  res.json({
    name: bestFood.name,
    ingredients: bestFood.ingredients,
    cooktime: calcTime(bestFood.duration, day, data),
  });
};
