const convertToMinute = (time) => {
  const [hour, minute] = time.split(":");
  return +hour * 60 + +minute;
};

const calcTime = (duration, day, data) => {
  const { timings } = data[day - 1];
  const asrTime = timings.Asr.split(" ")[0];
  const maghribTime = timings.Maghrib.split(" ")[0];
  const startTime = convertToMinute(maghribTime) - 15 - duration;
  const durationBetween = Math.abs(startTime - convertToMinute(asrTime));
  //compare it to asr prayer
  return `${durationBetween} minutes ${
    startTime > convertToMinute(asrTime) ? "after" : "before"
  } Asr`;
};
module.exports = { calcTime, convertToMinute };
