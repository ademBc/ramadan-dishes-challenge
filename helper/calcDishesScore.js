const ingredientInfo = require("../data/ingredientInfo.json");
const ALLOWED_CALORIES = 620;
const calcTotal = (food, field) => {
  return food.ingredients.reduce((acc, curr) => {
    const value = ingredientInfo.find(
      (ing) => ing.name.toLowerCase() === curr.toLowerCase()
    )[field];
    return acc + value;
  }, 0);
};
const calcScore = (food, day) => {
  const totalMoney = calcTotal(food, "price");
  const totalCalories = calcTotal(food, "calorie");
  const caloriesGap = Math.abs(totalCalories - ALLOWED_CALORIES);
  if (day > 19 && food.duration > 60) return -999999999;
  return -food.duration - totalMoney * -caloriesGap - food.ingredients.length;
};
module.exports = { calcScore, calcTotal };
